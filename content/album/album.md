---
widget: slider
headless: true  # This file represents a page section.
weight: 95

# ... Put Your Section Options Here (section position etc.) ...
# Optional header image (relative to `static/media/` folder).

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval: 3000

# Minimum slide height.
# Specify a height to ensure a consistent height for each slide.
height: 600px


item:
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-201910.png'
    overlay_filter: 0
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-2019cs.png'
    overlay_filter: 0
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-2020annual.png'
    overlay_filter: 0
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-20200724.png'
    overlay_filter: 0
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-202010.png'
    overlay_filter: 0
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-202012.png'
    overlay_filter: 0
  - title: ''
    content: ''
    align: center
    overlay_color: ''
    overlay_img: 'album-20200909.png'
    overlay_filter: 0
  - title: ""
    content: ''
    align: left
    overlay_color: ''
    overlay_img: 'album-20210716.png'
    overlay_filter: 0

---