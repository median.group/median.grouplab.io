---
title: Multi-Task Learning
summary: Multi-task learning(`MTL`) is a subfield of machine learning in which multiple learning tasks are solved at the same time, while exploiting commonalities and differences across tasks. This can result in improved learning efficiency and prediction accuracy for the task-specific models, when compared to training the models separately.
tags:
- Deep Learning
date: "2021-04-29T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://arxiv.org/abs/1707.08114v3

image:
  caption: Photo from Internet
  focal_point: Smart

url_pdf: "https://arxiv.org/abs/1707.08114v3"
---
Multitask Learning is an approach to inductive transfer that improves generalization by using the domain information contained in the training signals of related tasks as an inductive bias. It does this by learning tasks in parallel while using a shared representation; what is learned for each task can help other tasks be learned better.

A updated constantly survey given by Yu Zhang is available at [ArXiv](https://arxiv.org/abs/1707.08114v3).