---
# Display name
title: Lei Liu(刘蕾)

# Username (this should match the folder name)
authors:
- liulei

# Is this the primary user of the site?
superuser: false

# Role/position
role: Assistant

# Organizations/Affiliations
#organizations:
#- name: Southern University of Science and Technology
#  url: ""

# Short bio (displayed in user profile at end of posts)
bio: Assistant @ MEDIAN Group

#interests:
#- artificial intelligence
#- machine learning
#- pattern recognition
#- data mining


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:XXXX [at] mail [dot] sustech [dot] edu [dot] cn'
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to 
# `[Faculty, Postdoctor, Ph.D Student, Master Student, Research Assistant, Visiting Student, Alumni]` or comment out if you are not using People widget.

user_groups:
- Research Assistant
---