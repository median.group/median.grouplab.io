---
# Display name
title: MEDIAN Group

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Yu Zhang's Lab @ SUSTech

# Organizations/Affiliations to show in About widget
organizations:
- name: Department of Computer Science and Technology
  url: http://cse.sustech.edu.cn/
- name: Southern University of Science and Technology
  url: https://www.sustech.edu.cn/


# Short bio (displayed in user profile at end of posts)
bio: Our research interests include multi-task learning, transfer learning, deep learning, semi-supervised learning...

# Interests to show in About widget
interests:
- Multi-task Learning
- Transfer Learning
- Meta-learning
- Natural Language Processing

# Education to show in About widget
#Courses:
#  courses:
#  - course: PhD in Artificial Intelligence
#    institution: Stanford University
#    year: 2012
#  - course: MEng in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2009
#  - course: BSc in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '/#contact'
- icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
  icon_pack: fas
  link: https://scholar.google.com.hk/citations?user=jaRS5w4AAAAJ&hl=zh-CN
- icon: github
  icon_pack: fab
  link: https://github.com/median-lab

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---
The huMan-inspirEd Data-drIven Artificial iNtelligence (MEDIAN) Group was established in June 2019. It consists of a team of Dynamic, Diverse, Diligent members who devote themselves to Optimistic, Organized, Open-minded research life.

[Yu Zhang](https://faculty.sustech.edu.cn/zhangy7/) is an associate professor at the Southern University of Science and Technology, Shenzhen, China. His research interests include artificial intelligence, machine learning, pattern recognition, and data mining. He leads the MEDIAN Group, which develops methods for machine learning problems.

