---
widget: slider
headless: true  # This file represents a page section.
weight: 95
# ... Put Your Section Options Here (section position etc.) ...

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval: 3000

# Minimum slide height.
# Specify a height to ensure a consistent height for each slide.
height: 600px


item:
  - title: ''
    content: ''
    # Choose `center`, `left`, or `right` alignment.
    align: center
    # Overlay a color or image (optional).
    #   Deactivate an option by commenting out the line, prefixing it with `#`.
    overlay_color: '#666'  # An HTML color value.
    overlay_img: album-sustech.png  # Image path relative to your `assets/media/` folder
    overlay_filter: 1 # Darken the image. Value in range 0-1.
    # Call to action button (optional).
    #   Activate the button by specifying a URL and button label below.
    #   Deactivate by commenting out parameters, prefixing lines with `#`.
    cta_label: Go To CSE-SUSTech
    cta_url: 'http://cse.sustech.edu.cn/'
    cta_icon_pack: fas
    cta_icon: graduation-cap
  - title: ''
    content: ''
    # Choose `center`, `left`, or `right` alignment.
    align: center
    # Overlay a color or image (optional).
    #   Deactivate an option by commenting out the line, prefixing it with `#`.
    overlay_color: '#666'  # An HTML color value.
    overlay_img: album-sustech-2.png  # Image path relative to your `assets/media/` folder
    overlay_filter: 1 # Darken the image. Value in range 0-1.
    # Call to action button (optional).
    #   Activate the button by specifying a URL and button label below.
    #   Deactivate by commenting out parameters, prefixing lines with `#`.
    cta_label: Go To SUSTech Homepage
    cta_url: 'https://www.sustech.edu.cn/'
    cta_icon_pack: fas
    cta_icon: graduation-cap
---