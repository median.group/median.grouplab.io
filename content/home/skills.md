---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

title: 
subtitle:

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- icon: "battery-full"
  icon_pack: fas
  name: Dynamic
- icon: globe
  icon_pack: fas
  name: Diverse
- icon: brain
  icon_pack: fas
  name: Diligent
- icon: ":muscle:"
  icon_pack: "emoji"
  name: "Optimistic"
- icon: ":mortar_board:"
  icon_pack: "emoji"
  name: "Organized"
- icon: ":key:"
  icon_pack: "emoji"
  name: "Open-minded"


# Uncomment to use emoji icons.
#- icon: ":smile:"
#  icon_pack: "emoji"
#  name: "Emojiness"
#  description: "100%"  

# Uncomment to use custom SVG icons.
# Place custom SVG icon in `assets/images/icon-pack/`, creating folders if necessary.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
#- icon: "your-custom-icon-name"
#  icon_pack: "custom"
#  name: "Surfing"
#  description: "90%"
---
