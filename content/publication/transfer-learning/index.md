---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Transfer Learning"
authors: 
- Qiang Yang
- Yu Zhang
- Wenyuan Dai
- Sinno Jialin Pan
date: 2020-04-28T21:29:16+08:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2020-04-28T21:29:16+08:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["5"]

# Publication name and optional abbreviated publication name.
publication: "Cambridge University Press, ISBN 9781107016903"
publication_short: ""

abstract: "Transfer learning deals with how systems can quickly adapt themselves to new situations, tasks and environments. It gives machine learning systems the ability to leverage auxiliary data and models to help solve target problems when there is only a small amount of data available. This makes such systems more reliable and robust, keeping the machine learning model faced with unforeseeable changes from deviating too much from expected performance. At an enterprise level, transfer learning allows knowledge to be reused so experience gained once can be repeatedly applied to the real world."

# Summary. An optional shortened abstract.
summary: ""

tags: [Book, Transfer Learning]
categories: []
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: https://www.cambridge.org/hk/academic/subjects/computer-science/pattern-recognition-and-machine-learning/transfer-learning#titleAwards
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
{{% callout note %}}
Choice Outstanding Academic Title 2020, Choice Reviews
{{% /callout %}}

{{% callout note %}}
A Chinese Translation has been published by China Machine Press with ISBN 9787111661283.
{{% /callout %}}

{{% callout note %}}
Chinese Version is available at [JD.com](https://item.jd.com/12930984.html)
{{% /callout %}}