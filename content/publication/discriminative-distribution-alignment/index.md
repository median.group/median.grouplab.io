---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Discriminative Distribution Alignment: A Unified Framework for Heterogeneous Domain Adaptation"
authors:
- Yuan Yao
- Yu Zhang
- Xutao Li
- Yunming Ye
date: 2020-05-09T09:36:16+08:00
doi: "10.1016/j.patcog.2019.107165"

# Schedule page publish date (NOT publication's date).
publishDate: 2020-05-09T09:36:16+08:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Pattern Recognition (PR)*"
publication_short: "*PR*"

abstract: "Heterogeneous domain adaptation (HDA) aims to leverage knowledge from a source domain for helping learn an accurate model in a heterogeneous target domain. HDA is exceedingly challenging since the feature spaces of domains are distinct. To tackle this issue, we propose a unified learning framework called Discriminative Distribution Alignment (DDA) for deriving a domain-invariant subspace. The proposed DDA can simultaneously match the discriminative directions of domains, align the distributions across domains, and enhance the separability of data during adaptation. To achieve this, DDA trains an adaptive classifier by both reducing the distribution divergence and enlarging distances between class centroids. Based on the proposed DDA framework, we further develop two methods, by embedding the cross-entropy loss and squared loss into this framework, respectively. We conduct experiments on the tasks of categorization across domains and modalities. Experimental results clearly demonstrate that the proposed DDA outperforms several state-of-the-art models."

# Summary. An optional shortened abstract.
summary: ""

tags: [2020, Domain Adaptation]
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
