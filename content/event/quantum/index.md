---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Quantum Mechanics & Quantum Computing"
event:
event_url:
location: South Tower, College of Engineering
address: 
  street: SUSTECH
  city: Shenzhen
  region: Guangdong
  postcode: '518055'
  country: China
summary: An MEDIAN members ONLY seminar given by Jianghan Bao.
abstract: Brief introduce about Quantum computing & Quantum Machine Learning.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-04-29T9:30:00Z"
date_end: "2021-04-29T11:30:00Z"
all_day: false

# Schedule page publish date (NOT event date).
publishDate: 2021-04-28T17:06:40+08:00

authors: []
tags: []

# Is this a featured event? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your event's folder or a URL.
url_slides: "logo.pptx"
url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this event with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
