---
title: Regular Seminar

event: Weekly Seminar @ SUSTech
event_url: https://zhangyu601.quickconnect.cn/

location: South Tower, College of Engineering
address: 
  street: SUSTech
  city: Shenzhen
  region: Guangdong
  postcode: '518055'
  country: China
summary: Regular seminar.
abstract: MEDIAN Group members only. Please pay attention to the notice for the specific time.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2020-06-01T14:00:00Z"
date_end: "2020-06-01T17:30:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2020-06-01T00:00:00Z"

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/sNwnjxm8eTY)'
  focal_point: Right

url_code: ""
url_pdf: ""
url_slides: "http://10.16.11.95:3385/"
url_video: ""


# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects:
---

Slides & papers can be found in our Cloud Server.