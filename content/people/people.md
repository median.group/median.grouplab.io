---
# An instance of the People widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: people

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 50

title: Meet the Team
subtitle:
content:
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups:
    - Faculty
    - Postdoctor
    - Ph.D Student
    - Master Student
    - Research Assistant
    - Visiting Student
    - Alumni
design:
  # Show user's social networking links? (true/false)
  show_social: true
  # Show user's interests? (true/false)
  show_interests: true
  # Show user's role?
  show_role: true
  # Show user's organizations/affiliations?
  show_organizations: true
---