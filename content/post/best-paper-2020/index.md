---
title: An Overview of Multi-Task Learning" won the best paper award of National Science Review in 2020
subtitle: National Science Review, NSR

# Summary for listings and search engines
summary: National Science Review, NSR Best paper in 2020

# Link this post with a project
projects: ["multi-task"]

# Date published
date: "2021-03-13T00:00:00Z"

# Date updated
lastmod: "2021-03-13T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  focal_point: ""
  placement: 2
  preview_only: false
links:
- icon: weixin
  icon_pack: fab
  name: Wechat
  url: https://mp.weixin.qq.com/s/L7NNBnnpuPiCHM_mLoA4wA
- icon: link
  icon_pack: fas
  name: Link
  url: https://academic.oup.com/nsr/article/5/1/30/4101432
- icon: link
  icon_pack: fas
  name: Chinese Version
  url: https://www.jiqizhixin.com/articles/nsr-jan-2018-yu-zhang-qiang-yang
authors:
- admin

tags:
- Survey
- Multi-task Learning

categories:
- News
---

