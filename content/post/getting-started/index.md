---
title: The survey paper on multi-task learning got accepted by IEEE TKDE
subtitle: IEEE Transactions on Knowledge and Data Engineering.

# Summary for listings and search engines
summary: TKDE

# Link this post with a project
projects: ["multi-task"]

# Date published
date: "2021-03-21T00:00:00Z"

# Date updated
lastmod: "2021-03-21T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
  focal_point: ""
  placement: 2
  preview_only: false
links:
- icon: link
  icon_pack: fas
  name: Link
  url: http://arxiv.org/abs/2006.07622
authors:
- admin

tags:
- Survey
- Multi-task Learning

categories:
- News
---

