---
title: One paper got accepted by AAAI 2021
subtitle: Distant Transfer Learning via Deep Random Walk

# Summary for listings and search engines
summary: Distant Transfer Learning via Deep Random Walk

# Link this post with a project
projects: ["transfer-learning"]

# Date published
date: "2021-03-21T00:00:00Z"

# Date updated
lastmod: "2021-03-21T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: ''
  focal_point: ""
  placement: 2
  preview_only: false
links:
- icon: link
  icon_pack: fas
  name: Link
  url: http://arxiv.org/abs/1707.08114v3
authors:
- admin

tags:
- Transfer Learning

categories:
- News
---

